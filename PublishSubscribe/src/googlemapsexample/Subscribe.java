package googlemapsexample;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hr.fer.tel.pubsub.artefact.TripletSubscription;
import hr.fer.tel.pubsub.common.Triplet;
import hr.fer.tel.pubsub.entity.NotificationListener;
import hr.fer.tel.pubsub.entity.Subscriber;
import hr.fer.tel.pubsub.artefact.Publication;
import hr.fer.tel.pubsub.common.ReadingWritingXML;
import hr.fer.tel.pubsub.artefact.HashtablePublication;
import gmapstatic.MapMarker;

public class Subscribe {
	
	private String 		brokIP 		= "193.10.227.204";
	private int 		brokPort 			= 6237;
	List<String> markers;
	Listener theListener;
	
	public void estConn(){
		
		Scanner keyboard = new Scanner(System.in);
		while(true){
			System.out.println("Use predefined Settings, broker IP/Port: " + brokIP + " / " + brokPort + " (y/n)?");
			String c = keyboard.nextLine();
			
			if(c.equals("n")){ setIP(keyboard); setPort(keyboard); break;}
			else if(c.equals("y")) break;
		}
		
		//keyboard.close();
		
		Subscriber theSubscriber = new Subscriber ( "Subscriber1","PNALGORITHM",getIP(),getPort() );
		theSubscriber.setLogWriting( false );
		// Don�'t print subscriptions to screen.
		theSubscriber.setTesting( true );
		// Connect with MoPS broker.
		theSubscriber.connect();
		// Publications are catched by a listener.
		theListener = new Listener();
		theSubscriber.setNotificationListener( theListener );
		createSubscriptions(theSubscriber);
		
		
	}
	
	private void createSubscriptions(Subscriber theSubscriber){
		
		Scanner keyboard = new Scanner(System.in);
		
		while(true){
		// Create a new subscription.
			TripletSubscription theSubscription = new TripletSubscription();
		// Start the subscription directly.
			theSubscription.setStartTime( System.currentTimeMillis() );
		// Time of validity: 30 seconds.
			theSubscription.setValidity(System.currentTimeMillis() + 30000 );
			System.out.println("Input BusNo to subscribe to (n to quit) ");
			String busID = keyboard.nextLine();
			if(busID.equals("n")){
				break;
			}
			theSubscription.setProperty( new Triplet( "BusID", busID, "=" ) );
		// Send subscription to MoPS broker.
			theSubscriber.subscribe( theSubscription);
		}
		keyboard.close();
	}
	
	public MapMarker[] getMarkers(){
		 
		return theListener.getMarkers();
		
	}
	
	
	private void setIP(Scanner keyboard){
		
		System.out.println("Enter broker IP: ");
		brokIP = keyboard.nextLine();
			
	}
	
	private void setPort(Scanner keyboard){
		
		System.out.println("Enter port: ");
		brokPort = keyboard.nextInt();
		
	}
	
	private String getIP(){
		
		return brokIP;
	}
	
	private int getPort(){
		
		return brokPort;
	}
	
/*########################LISTENER CLASS#############################*/
	
	
	private class Listener extends NotificationListener {

		String[] parts;
		Subscribe sub;
		ArrayList<MapMarker> markerList;
		
		public Listener(){
			
			markerList = new ArrayList<MapMarker>();
			
		}
		
		public void notify(java.util.UUID subscriberId, String subscriberName, Publication notification) {
			
			System.out.println("Notication from " + subscriberName);
			System.out.println(subscriberName);
			setTracker((HashtablePublication) notification);
			
		}
		
		private void setTracker(HashtablePublication publication){
			
			String properties = publication.getProperties().toString();
			properties = properties.substring(1, properties.length()-1);
			System.out.println(properties);
			parts = properties.split(", ");
			for(int i=0; i < parts.length; i++){
				parts[i]=parts[i].replaceAll(".*=","");
				System.out.println(parts[i]);
			}
			addMarker(new MapMarker(Double.parseDouble(parts[1]),Double.parseDouble(parts[2])));
			
		}
		
		private void addMarker(MapMarker mapMarker){
			
			markerList.add(mapMarker);
			
		}
		
		public MapMarker[] getMarkers(){
			
			MapMarker[] markerArray = new MapMarker[markerList.size()];
			
			markerArray = markerList.toArray(markerArray);
			markerList = new ArrayList<MapMarker>();
			
			return markerArray;
			
		}
	}

}


