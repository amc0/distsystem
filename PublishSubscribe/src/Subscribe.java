import hr.fer.tel.pubsub.artefact.TripletSubscription;
import hr.fer.tel.pubsub.common.Triplet;
import hr.fer.tel.pubsub.entity.NotificationListener;
import hr.fer.tel.pubsub.entity.Subscriber;
import hr.fer.tel.pubsub.artefact.Publication;
import hr.fer.tel.pubsub.common.ReadingWritingXML;
import hr.fer.tel.pubsub.artefact.HashtablePublication;

public class Subscribe {

	Subscriber theSubscriber;
	TripletSubscription theSubscription;
	
	public void estConn(){
		theSubscriber = new Subscriber ( "Subscriber1","PNALGORITHM","193.10.227.204",6237 );
		theSubscriber.setLogWriting( false );
		// Don�'t print subscriptions to screen.
		theSubscriber.setTesting( true );
		// Connect with MoPS broker.
		theSubscriber.connect();
		// Publications are catched by a listener.
		Listener theListener = new Listener();
		theSubscriber.setNotificationListener( theListener );
		// Create a new subscription.
		theSubscription = new TripletSubscription();
		// Start the subscription directly.
		theSubscription.setStartTime( System.currentTimeMillis() );
		// Time of validity: 30 seconds.
		theSubscription.setValidity(System.currentTimeMillis() + 30000 );
		theSubscription.setProperty( new Triplet( "BusID", "y2020", "=" ) );
		// Send subscription to MoPS broker.
		theSubscriber.subscribe( theSubscription );
		
	}
	
	

	private class Listener extends NotificationListener {

		public void notify(java.util.UUID subscriberId, String subscriberName, Publication notification) {
			System.out.println("Notication from " + subscriberName);
			
			HashtablePublication publication = (HashtablePublication) notification;
			System.out.println(publication.getProperties());
		}

	}
}


