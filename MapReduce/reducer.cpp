#include <string>
#include <list>
#include <fstream>
#include <map>
#include <stdio.h>
#include <iostream>

using namespace std;

typedef map<string, string> HashTable_t;
typedef HashTable_t::iterator HashTableIter_t;
string tmp;


bool compare_nocase (const std::string& first, const std::string& second)
{
  unsigned int i=0;
  
  while ( (i<first.length()) && (i<second.length()) )
  {
    if (tolower(first[i])<tolower(second[i])) return true;
    else if (tolower(first[i])>tolower(second[i])) return false;
    ++i;
  }
  return ( first.length() < second.length() );
}

string strip(string delimiter, string text){
	
	size_t pos = 0;
	string theWord;
	while((pos = text.find(delimiter)) != string::npos){
		theWord = text.substr(0, pos);
		text.erase(0,pos + delimiter.length());
	}
		
	return theWord;
	
}

int main(){

	/*list <string> words;
	list<string>::iterator i;
    string tmp;*/
    
    HashTable_t theHashTable;
    string tabDelimiter = "\t";
    string fileDelimiter = ".txt";
    string theWord;
   list <string> words;
   list<string>::iterator it;
	
	do{
		getline(cin,tmp);
		words.push_back(tmp);  
		//cout << tmp <<endl;		Remove comment to see list
	}
	while(cin);
   
   //words.sort(compare_nocase);
   
   for(it=words.begin(); it != words.end(); ++it){
		
		string prevFile;
		tmp = *it;
		
		size_t pos = 0;
		while((pos = tmp.find(tabDelimiter)) != string::npos){
				theWord = tmp.substr(0,pos);
				tmp.erase(0,pos+tabDelimiter.length());
		}
		theHashTable[ theWord ] += tmp + ' ';
	}
	
	
	for( HashTableIter_t 	it = theHashTable.begin(); 
							it != theHashTable.end();
							++it){
		cout << it->first << "\t" << it->second << endl;
	}
		
	
	return 0;

}
