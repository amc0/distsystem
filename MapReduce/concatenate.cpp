#include <string>
#include <list>
#include <fstream>
#include <stdio.h>
#include <iostream>

using namespace std;

bool compare_nocase (const std::string& first, const std::string& second)
{
  unsigned int i=0;
  while ( (i<first.length()) && (i<second.length()) )
  {
    if (tolower(first[i])<tolower(second[i])) return true;
    else if (tolower(first[i])>tolower(second[i])) return false;
    ++i;
  }
  return ( first.length() < second.length() );
}

int concat(string file1, string file2, string file3){

        ifstream fIn1, fIn2;
        ofstream fOut;

        fIn1.open(file1.c_str());
        if(!fIn1.good()) throw "I/O Error";
        fIn2.open(file2.c_str());
        if(!fIn2.good()) throw "I/O2 Error";
        fOut.open(file3.c_str(),ios::app);
        if(!fOut.good()) throw "I/O3 Error";

        if((fIn1!=NULL) && (fIn2!=NULL) && (fOut!=NULL)){

            cout << file1 << endl;

            list <string> words;
            string tmp;

            while(getline(fIn1,tmp)){
                words.push_back(tmp);
            }
            tmp.clear();
            while(getline(fIn2,tmp)){
                words.push_back(tmp);
            }
            list<string>::iterator i;
            for(i=words.begin(); i != words.end(); i++){
                cout << *i << endl;
            }

            words.sort(compare_nocase);

            for(i=words.begin(); i != words.end(); i++){
                cout << *i << endl;
                fOut << *i << endl;
            }
        }
        else
            cout << "Couldn't open necessary files" << endl;

        return 0;
}

int main(){
    concat("Out.txt","Out2.txt","Reduce.txt");
}
