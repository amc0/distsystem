#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>

using namespace std;

int main(){

   string tmp, filename=getenv("map_input_file");
   int pos = 0;
   
	const size_t last_slash_idx = filename.find_last_of("\\/");
	if (std::string::npos != last_slash_idx)
	{
		filename.erase(0, last_slash_idx + 1);
	}
   
   while(cin  >> tmp){
       cout <<  tmp << "\t" << filename << ':' << pos << endl;
       pos+=tmp.size()+1;
    }

    return 0;

}
