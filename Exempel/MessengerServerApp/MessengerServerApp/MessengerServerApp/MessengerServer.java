
/**
 * Karl-Johan Grinnemo, Dept. Computer Science, Karlstad University July 5, 2013
 * Simple instant messenger application in course DVGC15.
 */


import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class MessengerServer implements MessageListener {

    private ExecutorService serverExecutor;

    public void startServer() {
        serverExecutor = Executors.newCachedThreadPool();

	// CODE MISSING.
    }

    // when new message is received, broadcast message to clients
    public void messageReceived(String from, String message) {
        // create String containing entire message
        String completeMessage = from + 
                SocketMessengerConstants.MESSAGE_SEPARATOR + message;

        // create and start MulticastSender to broadcast messages
        serverExecutor.execute(new MulticastSender(completeMessage.getBytes()));
    }
}