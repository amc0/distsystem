/**
 * Karl-Johan Grinnemo, Dept. Computer Science, Karlstad University July 5, 2013
 * Simple instant messenger application in course DVGC15.
 */


public class MessengerServerApp {

    public static void main(String args[]) {
        MessengerServer application = new MessengerServer();
        application.startServer();
    }
}
