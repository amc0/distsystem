
/**
 * Karl-Johan Grinnemo, Dept. Computer Science, Karlstad University July 5, 2013
 * Simple instant messenger application in course DVGC15.
 */


import java.net.*;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.xml.bind.Marshaller.Listener;

public class SocketMessageManager implements MessageManager,SocketMessengerConstants {

	private byte[] fromByte;
	private byte[] msgByte;
    private DatagramSocket clientSocket;            // Socket for outgoing message
    private DatagramPacket pack;
    private InetAddress group;
    private MessageChecker msgChk;
    private MulticastSocket multicastSock;
    private String serverAddress;           // MessengerServer address
    private int serverPort;
    private boolean connected = false;      // connection status
    private ExecutorService serverExecutor; // executor for server

    public SocketMessageManager(String address) {
    	serverAddress = address;
    	try { group = InetAddress.getByName(serverAddress);	} 
    	catch (UnknownHostException e) { e.printStackTrace(); }
    	
        serverAddress = address; // store server address
        serverExecutor = Executors.newCachedThreadPool();
        
    }

    // connect to server and send messages to given MessageListener
    public void connect(MessageListener listener) {
    	//serverExecutor = Executors.newCachedThreadPool();
        if (connected) {
            return; // if already connected, return immediately
        }
        else{
        	try { multicastSock = new MulticastSocket(MULTICAST_LISTENING_PORT); }
        	catch (IOException e1) { e1.printStackTrace(); }
        	
        	try { multicastSock.joinGroup(group); }
        	catch (IOException e) { e.printStackTrace(); }
        	
        	serverPort = MULTICAST_SENDING_PORT;
        	connected = true;
        	
        	/*try { multicastSock.receive(pack); } 
        	catch (IOException e) { e.printStackTrace(); }*/
        	
        	listener.messageReceived("User", "Connected");
        	msgChk = new MessageChecker(listener,multicastSock);
        	serverExecutor.execute(msgChk);
        	
        	
        }
	// CODE MISSING.

    }

    // disconnect from server and unregister given MessageListener
    public void disconnect(MessageListener listener) {
        if (!connected) {
            return; // if not connected, return immediately
        }
        else{

        	msgChk.disconnect();
        	
        	try { multicastSock.leaveGroup(group); }
        	catch (IOException e) { e.printStackTrace(); System.out.println("Couldn't disconnect!");}
        	
        	
        	/*serverExecutor.shutdown();
        	serverExecutor.shutdownNow();*/
        	
        	serverAddress = null;
        	serverPort = 0;
        	connected = false;
        	//multicastSock.close();
        	
        }

    }

    // send message to server
    public void sendMessage(String from, String message) {
    	
        if (!connected) {
            return; // if not connected, return immediately
        }
        else{
        	fromByte = from.getBytes();
        	msgByte = message.getBytes();
        	castMessage(multicastSock);
        }   	

    }
    
    public void castMessage(MulticastSocket sock){
    try{	
    	pack = new DatagramPacket(fromByte,fromByte.length,group,serverPort);		//Sends the from string
		sock.send(pack);
		
		pack = new DatagramPacket(msgByte,msgByte.length,group,serverPort);			//Sends the message
		sock.send(pack);
    	
	    } catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

	private class MessageChecker implements Runnable {
	
	    private MessageListener listen;    // user from which message came
	    private MulticastSocket multSock; // body of message
	    private DatagramPacket pack;
	    private Boolean connState;
	    
	    // MessageDisplayer constructor
	    public MessageChecker(MessageListener listener, MulticastSocket sock) {
	    	listen = listener;
	    	multSock = sock;
	    	connState = true;
	    }
	    
	    public void disconnect(){
	    	connState = false;
	    	System.out.println("Disconnect");
	    }

		@Override
		public void run() {
			// TODO Auto-generated method stub
			System.out.println("State: " + connState);
			while(true){
				String message = null;
				if(!connState){
					multicastSock.close();
					System.out.println("User disconnected");
					break;
				}
				else{
					byte[] buf = new byte[1024];
				    pack = new DatagramPacket(buf, buf.length);
					
				    try {
				    	multSock.receive(pack);
					} catch (IOException e) { e.printStackTrace(); }
	
				    message = new String(pack.getData(),0,pack.getLength());
				    System.out.println("Message: " + message);
				    
				    listen.messageReceived("",message);
				}
			}
		}
	}	 
}
