package MessengerServerApp.MessengerServerApp;

/**
 * Karl-Johan Grinnemo, Dept. Computer Science, Karlstad University July 5, 2013
 * Simple instant messenger application in course DVGC15.
 */


import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class MulticastSender implements Runnable {

    private byte[] messageBytes; // message data

    public MulticastSender(byte[] bytes) {
        messageBytes = bytes; // create the message
    }

    // deliver message to MULTICAST_ADDRESS over DatagramSocket
    public void run() {
        try // deliver message
        {
            // create DatagramSocket for sending message
            DatagramSocket socket = 
                    new DatagramSocket(SocketMessengerConstants.MULTICAST_SENDING_PORT);

            // use InetAddress reserved for multicast group
            InetAddress group = 
                    InetAddress.getByName(SocketMessengerConstants.MULTICAST_ADDRESS);

            // create DatagramPacket containing message
            DatagramPacket packet = new DatagramPacket(messageBytes,
                    messageBytes.length,
                    group,
                    SocketMessengerConstants.MULTICAST_LISTENING_PORT);

            socket.send(packet); // send packet to multicast group
            socket.close();      // close socket
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
