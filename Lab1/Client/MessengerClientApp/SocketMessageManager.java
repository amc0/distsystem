package MessengerClientApp;
/**
 * Karl-Johan Grinnemo, Dept. Computer Science, Karlstad University July 5, 2013
 * Simple instant messenger application in course DVGC15.
 */


import java.net.InetAddress;
import java.net.Socket;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class SocketMessageManager implements MessageManager {

    private Socket clientSocket;            // Socket for outgoing messages
    private String serverAddress;           // MessengerServer address
    private boolean connected = false;      // connection status
    private ExecutorService serverExecutor; // executor for server

    public SocketMessageManager(String address) {
        serverAddress = address; // store server address
        serverExecutor = Executors.newCachedThreadPool();
    }

    // connect to server and send messages to given MessageListener
    public void connect(MessageListener listener) {
        if (connected) {
            return; // if already connected, return immediately
        }

	// CODE MISSING.

    }

    // disconnect from server and unregister given MessageListener
    public void disconnect(MessageListener listener) {
        if (!connected) {
            return; // if not connected, return immediately
        }

	// CODE MISSING.

    }

    // send message to server
    public void sendMessage(String from, String message) {
        if (!connected) {
            return; // if not connected, return immediately
        }

	// CODE MISSING.

    }
}
