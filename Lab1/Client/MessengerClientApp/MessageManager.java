package MessengerClientApp;
/**
 * Karl-Johan Grinnemo, Dept. Computer Science, Karlstad University July 5, 2013
 * Simple instant messenger application in course DVGC15.
 */


public interface MessageManager {
    
    // connect to message server and route incoming messages
    // to given MessageListener
    public void connect(MessageListener listener);

    // disconnect from message server and stop routing
    // incoming messages to given MessageListener
    public void disconnect(MessageListener listener);

    // send message to message server
    public void sendMessage(String from, String message);
}
