package MessengerClientApp;

/**
 * Karl-Johan Grinnemo, Dept. Computer Science, Karlstad University July 5, 2013
 * Simple instant messenger application in course DVGC15.
 */


public class MessengerClientApp {

    public static void main(String args[]) {
        MessageManager messageManager; // declare MessageManager

        if (args.length == 0) // connect to localhost
        {
            messageManager = new SocketMessageManager("localhost");
        } else // connect using command-line arg
        {
            messageManager = new SocketMessageManager(args[ 0]);
        }

        // create GUI for SocketMessageManager
        ClientGUI clientGUI = new ClientGUI(messageManager);
        clientGUI.setSize(300, 400);   // set window size
        clientGUI.setResizable(false); // disable resizing
        clientGUI.setVisible(true);    // show window
    }
}
