/**
 * Karl-Johan Grinnemo, Dept. Computer Science, Karlstad University
 * July 5, 2013
 * Simple instant messenger application in course DVGC15.
 */


public interface MessageListener {
    // receive new chat message
    public void messageReceived(String from, String message);
}
