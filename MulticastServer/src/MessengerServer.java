
/**
 * Karl-Johan Grinnemo, Dept. Computer Science, Karlstad University July 5, 2013
 * Simple instant messenger application in course DVGC15.
 */

import java.net.*;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class MessengerServer implements MessageListener,SocketMessengerConstants {

	private DatagramPacket pack;
    private ExecutorService serverExecutor;
    private InetAddress group;
    private MulticastSocket multicastSock;
    
    public void startServer() {
        serverExecutor = Executors.newCachedThreadPool();
        
        try {
			group = InetAddress.getByName(MULTICAST_ADDRESS);
		} catch (UnknownHostException e) { e.printStackTrace(); }
        
        try {
			multicastSock = new MulticastSocket(MULTICAST_SENDING_PORT);
		} catch (IOException e) { e.printStackTrace(); }
        
        try {
			multicastSock.joinGroup(group);
			System.out.println(multicastSock.toString());
		} catch (IOException e) { e.printStackTrace(); }
        
       	run(multicastSock);
        multicastSock.close();
        
	// CODE MISSING.
    }
    
    private void run(MulticastSocket sock){
    	while(true){
	    	byte[] buf = new byte[1024];
	        pack = new DatagramPacket(buf, buf.length);
	    	
	        try {
				sock.receive(pack);
			} catch (IOException e) { e.printStackTrace(); }

	        String from = new String(pack.getData(),0,pack.getLength());
	        System.out.println("From: " + from);
	        
	        try {
				sock.receive(pack);
			} catch (IOException e) { e.printStackTrace(); }

	        String message = new String(pack.getData(),0,pack.getLength());
	        System.out.println("Message: " + message);
	        
	        messageReceived(from,message);
    	}
    }

    // when new message is received, broadcast message to clients
    public void messageReceived(String from, String message) {
        // create String containing entire message
        String completeMessage = from + 
                SocketMessengerConstants.MESSAGE_SEPARATOR + message;

        // create and start MulticastSender to broadcast messages
        serverExecutor.execute(new MulticastSender(completeMessage.getBytes()));
    }
}